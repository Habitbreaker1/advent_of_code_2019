use crate::program::{Int, Program};

#[derive(Debug, PartialEq)]
pub enum Instruction {
    //  Store sum of P1 and P2 at P3
    //  ---------------
    // |ABC|01|P1|P2|P3|
    //  ---------------
    ADD(Parameter, Parameter, Parameter),

    // Store product of P1 and P2 at P3
    // ---------------
    // |ABC|02|P1|P2|P3|
    //  ---------------
    MUL(Parameter, Parameter, Parameter),

    // Store input at P1
    //  -------
    // |C|03|P1|
    //  -------
    ST(Parameter),

    // Load P1
    //  -------
    // |C|04|P1|
    //  -------
    LD(Parameter),

    // Jump to P2 if P1 != 0
    //  -----------
    // |BC|05|P1|P2|
    //  -----------
    JNZ(Parameter, Parameter),

    // Jump to P2 if P1 == 0
    //  -----------
    // |BC|06|P1|P2|
    //  -----------
    JZ(Parameter, Parameter),

    // Store 1 at P3 if P1 < P2
    //  --------------
    // |BC|07|P1|P2|P3|
    //  --------------
    STL(Parameter, Parameter, Parameter),

    // Store 0 at P3 if P1 == P2
    //  --------------
    // |ABC|08|P1|P2|P3|
    //  --------------
    STE(Parameter, Parameter, Parameter),

    // Add P1 to relative base
    //   --------
    //  |C|09|P1|
    //   --------
    RB(Parameter),

    // Halt program
    //  --
    // |99|
    //  --
    HALT,
}

impl Instruction {
    pub fn exec(&self, program: &mut Program) {
        use Instruction::*;

        match self {
            ADD(p1, p2, p3) => {
                program.store(
                    p3.get_store_addr(&program),
                    p1.load(&program) + p2.load(&program),
                );
                program.pc += 4;
            }
            MUL(p1, p2, p3) => {
                program.store(
                    p3.get_store_addr(&program),
                    p1.load(&program) * p2.load(&program),
                );
                program.pc += 4;
            }
            ST(p1) => {
                let val = program
                    .params
                    .pop()
                    .expect("ST called but no parms on the stack 🤷");

                program.store(p1.get_store_addr(&program), val);
                program.pc += 2;
            }
            LD(p1) => {
                let val = p1.load(&program);

                if let Some(out) = program.output.as_mut() {
                    out.push(val);
                } else {
                    program.output = Some(vec![val]);
                }

                program.pc += 2;
            }
            HALT => {}
            JNZ(p1, p2) => {
                if p1.load(&program) != 0 {
                    program.pc = p2.load(&program) as usize;
                } else {
                    program.pc += 3;
                }
            }
            JZ(p1, p2) => {
                if p1.load(&program) == 0 {
                    program.pc = p2.load(&program) as usize;
                } else {
                    program.pc += 3;
                }
            }
            STL(p1, p2, p3) => {
                let store_addr = p3.get_store_addr(&program);

                if p1.load(&program) < p2.load(&program) {
                    program.store(store_addr, 1);
                } else {
                    program.store(store_addr, 0);
                }
                program.pc += 4;
            }
            STE(p1, p2, p3) => {
                let store_addr = p3.get_store_addr(&program);

                if p1.load(&program) == p2.load(&program) {
                    program.store(store_addr, 1);
                } else {
                    program.store(store_addr, 0);
                }
                program.pc += 4;
            }
            RB(p1) => {
                program.relative_base += p1.load(&program);
                program.pc += 2;
            }
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum ParameterMode {
    Position,
    Immediate,
    Relative,
}

#[derive(Debug, PartialEq)]
pub struct Parameter {
    val: Int,
    mode: ParameterMode,
}

impl Parameter {
    pub fn new(val: Int, mode: ParameterMode) -> Self {
        Self { val, mode }
    }

    fn load(&self, program: &Program) -> Int {
        match self.mode {
            ParameterMode::Position => program.load(self.val as usize),
            ParameterMode::Immediate => self.val,
            ParameterMode::Relative => program.load((self.val + program.relative_base) as usize),
        }
    }

    fn get_store_addr(&self, program: &Program) -> usize {
        let addr = match self.mode {
            ParameterMode::Relative => self.val + program.relative_base,
            _ => self.val,
        };
        addr as usize
    }
}
