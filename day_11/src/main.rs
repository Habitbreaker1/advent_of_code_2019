use std::fs::File;
use std::io::prelude::*;
use std::io::Result;

mod instruction_set;
mod program;

use program::{Int, Program};

fn main() -> Result<()> {
    let mut file = File::open("input.txt")?;
    let mut buffer = String::new();
    file.read_to_string(&mut buffer)?;

    let mem: Vec<Int> = buffer
        .trim()
        .split(',')
        .map(|opcode| opcode.parse().unwrap())
        .collect();

    // 0: part 1, 1: part 2
    let start_code = 1;
    let mut program = Program::new(mem, vec![start_code]);

    let mut painted: Vec<(Point, Int)> = Vec::new();
    // Up=0, Right=1, Down=2, Left=3
    let directions = vec![0u8, 1, 2, 3];
    let mut dir_iter = directions.iter().cycle();

    // Start-direction is Up
    let mut curr_pos = Position::new(Point::new(0, 0), *dir_iter.next().unwrap());

    while let Some(color) = program.next_output() {
        let color = *color;
        let rotation = *program.next_output().expect("Expected direction");

        if let Some(idx) = painted.iter().position(|p| p.0 == curr_pos.coords) {
            painted[idx].1 = color;
        } else {
            painted.push((curr_pos.coords.clone(), color));
        }

        // Rotate robot
        // 1 rotation left equals 3 rotations right
        let next_dir = {
            if rotation == 0 {
                dir_iter.next();
                dir_iter.next();
            }
            dir_iter.next().unwrap()
        };
        let mut next_y = curr_pos.coords.y;
        let mut next_x = curr_pos.coords.x;

        match next_dir {
            0 => {
                next_y -= 1;
            }
            1 => {
                next_x += 1;
            }
            2 => {
                next_y += 1;
            }
            3 => {
                next_x -= 1;
            }
            _ => unreachable!(),
        };

        // Move robot
        curr_pos.coords.x = next_x;
        curr_pos.coords.y = next_y;
        curr_pos.dir = *next_dir;

        // Set next color instruction
        if let Some(painted) = painted.iter().find(|p| p.0 == curr_pos.coords) {
            program.add_param(painted.1);
        } else {
            program.add_param(0);
        }
    }
    println!("Points painted at least once: {}", painted.len());

    paint(painted);
    Ok(())
}

fn paint(points: Vec<(Point, Int)>) {
    let y_min = points.iter().map(|p| p.0.y).min().unwrap();
    let x_min = points.iter().map(|p| p.0.x).min().unwrap();
    let y_max = points.iter().map(|p| p.0.y).max().unwrap();
    let x_max = points.iter().map(|p| p.0.x).max().unwrap();

    let y_size = (y_max - y_min) as usize + 1;
    let x_size = (x_max - x_min) as usize + 1;

    let mut panel = vec![vec!['.'; x_size]; y_size];

    for point in points {
        let color = match point.1 {
            0 => '.',
            1 => '#',
            _ => unreachable!(),
        };
        panel[(point.0.y + y_min.abs()) as usize][(point.0.x + x_min.abs()) as usize] = color;
    }

    for row in panel {
        for col in row {
            print!("{}", col);
        }
        println!();
    }
}

#[derive(Debug, PartialEq, Clone)]
struct Position {
    coords: Point,
    dir: u8,
}

impl Position {
    fn new(coords: Point, dir: u8) -> Self {
        Self { coords, dir }
    }
}

#[derive(Debug, Eq, PartialEq, Clone, Hash)]
struct Point {
    x: Int,
    y: Int,
}

impl Point {
    fn new(x: Int, y: Int) -> Self {
        Self { x, y }
    }
}
