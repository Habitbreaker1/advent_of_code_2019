use crate::instruction_set::{Instruction, Parameter, ParameterMode};

pub type Int = i64;

pub struct Program {
    pub mem: Vec<Int>,
    pub pc: usize,
    // Params get loaded from right to left
    // [input, phase_setting] -> phase setting gets loaded first
    pub params: Vec<Int>,
    pub output: Option<Vec<Int>>,
    pub relative_base: Int,
}

#[allow(dead_code)]
impl Program {
    pub fn new(mem: Vec<Int>, params: Vec<Int>) -> Self {
        Self {
            mem,
            pc: 0,
            params,
            output: None,
            relative_base: 0,
        }
    }
    pub fn add_param(&mut self, p: Int) {
        self.params.push(p);
    }
    // --------
    // |ABC|DE|
    // -------------
    // |P3 P2 P1|Op|
    // -------------
    fn fetch(&self) -> Instruction {
        let pc = self.pc;
        let instr_str = format!("{:05}", self.mem[pc]);

        // Decode instruction
        let opcode = &instr_str[3..];
        let param_modes: Vec<ParameterMode> = instr_str
            .chars()
            .take(3)
            .map(|c| match c.to_digit(10).unwrap() {
                0 => ParameterMode::Position,
                1 => ParameterMode::Immediate,
                2 => ParameterMode::Relative,
                _ => unreachable!(),
            })
            .collect();

        match opcode {
            "01" => Instruction::ADD(
                Parameter::new(self.mem[pc + 1], param_modes[2]),
                Parameter::new(self.mem[pc + 2], param_modes[1]),
                Parameter::new(self.mem[pc + 3], param_modes[0]),
            ),
            "02" => Instruction::MUL(
                Parameter::new(self.mem[pc + 1], param_modes[2]),
                Parameter::new(self.mem[pc + 2], param_modes[1]),
                Parameter::new(self.mem[pc + 3], param_modes[0]),
            ),
            "03" => Instruction::ST(Parameter::new(self.mem[pc + 1], param_modes[2])),
            "04" => Instruction::LD(Parameter::new(self.mem[pc + 1], param_modes[2])),
            "05" => Instruction::JNZ(
                Parameter::new(self.mem[pc + 1], param_modes[2]),
                Parameter::new(self.mem[pc + 2], param_modes[1]),
            ),
            "06" => Instruction::JZ(
                Parameter::new(self.mem[pc + 1], param_modes[2]),
                Parameter::new(self.mem[pc + 2], param_modes[1]),
            ),
            "07" => Instruction::STL(
                Parameter::new(self.mem[pc + 1], param_modes[2]),
                Parameter::new(self.mem[pc + 2], param_modes[1]),
                Parameter::new(self.mem[pc + 3], param_modes[0]),
            ),
            "08" => Instruction::STE(
                Parameter::new(self.mem[pc + 1], param_modes[2]),
                Parameter::new(self.mem[pc + 2], param_modes[1]),
                Parameter::new(self.mem[pc + 3], param_modes[0]),
            ),
            "09" => Instruction::RB(Parameter::new(self.mem[pc + 1], param_modes[2])),
            "99" => Instruction::HALT,
            _ => unreachable!(),
        }
    }

    pub fn load(&self, addr: usize) -> Int {
        if let Some(val) = self.mem.get(addr) {
            *val
        } else {
            0
        }
    }

    pub fn store(&mut self, addr: usize, val: Int) {
        if addr >= self.mem.len() {
            self.mem.resize_with(addr + 1, Default::default);
        }
        self.mem[addr] = val;
    }

    pub fn run(&mut self) -> Option<&Vec<Int>> {
        let mut instruction = self.fetch();
        while instruction != Instruction::HALT {
            instruction.exec(self);
            instruction = self.fetch();
        }
        self.output.as_ref()
    }

    pub fn next_output(&mut self) -> Option<&Int> {
        let mut instruction = self.fetch();
        while instruction != Instruction::HALT {
            instruction.exec(self);

            if let Instruction::LD(_) = instruction {
                return self.output.as_ref().unwrap().last();
            }

            instruction = self.fetch();
        }
        None
    }
}
