use std::fs::File;
use std::io::prelude::*;
use std::io::Result;

fn main() -> Result<()> {
    let mut file = File::open("input.txt")?;

    let mut buffer = String::new();
    file.read_to_string(&mut buffer)?;

    let initial_state: Vec<usize> = buffer
        .split(",")
        .map(|opcode| opcode.parse().unwrap())
        .collect();

    let evaluate = |memory: &mut Vec<usize>, noun: usize, verb: usize| {
        memory[1] = noun;
        memory[2] = verb;
        let mut pc = 0; // Programm counter
        while memory[pc] != 99 {
            let a_addr = memory[pc + 1];
            let b_addr = memory[pc + 2];
            let store_addr = memory[pc + 3];

            match memory[pc] {
                1 => {
                    memory[store_addr] = memory[a_addr] + memory[b_addr];
                }
                2 => {
                    memory[store_addr] = memory[a_addr] * memory[b_addr];
                }
                _ => unreachable!(),
            }
            pc += 4;
        }
        memory[0]
    };

    // Part 1: Set noun=12 and verb=2
    println!("Output {}", evaluate(&mut initial_state.clone(), 12, 2));

    // Part 2: Bruteforce noun and verb for output 19690720
    let output = 19690720;
    println!("Bruteforce noun and verb for output: {}", output);

    'noun: for noun in 0..=99 {
        for verb in 0..=99 {
            if evaluate(&mut initial_state.clone(), noun, verb) == output {
                println!(
                    "Found noun: {} and verb: {} for output {}",
                    noun, verb, output
                );
                println!("100 * noun + verb = {}", noun * 100 + verb);
                break 'noun;
            }
        }
    }

    Ok(())
}
