use std::fs::File;
use std::io::prelude::*;
use std::io::Result;

fn main() -> Result<()> {
    let mut file = File::open("input.txt")?;
    let mut buffer = String::new();
    file.read_to_string(&mut buffer)?;

    let mut program = Program::new(
        buffer
            .split(",")
            .map(|opcode| opcode.parse().unwrap())
            .collect(),
        5, // Input for part 2
    );
    program.run();
    Ok(())
}

struct Program {
    mem: Vec<i32>,
    pc: usize,
    input: i32,
}

impl Program {
    fn new(mem: Vec<i32>, input: i32) -> Self {
        Self { mem, pc: 0, input }
    }

    // --------
    // |ABC|DE|
    // -------------
    // |P3 P2 P1|Op|
    // -------------
    fn fetch(&self) -> Instruction {
        let pc = self.pc;
        let instr_str = format!("{:05}", self.mem[pc]);

        // Decode instruction
        let opcode = &instr_str[3..];
        let param_modes: Vec<ParameterMode> = instr_str
            .chars()
            .take(3)
            .map(|c| match c.to_digit(10).unwrap() {
                0 => ParameterMode::Position,
                1 => ParameterMode::Immediate,
                _ => unreachable!(),
            })
            .collect();

        match opcode {
            "01" => Instruction::ADD(
                Parameter::new(self.mem[pc + 1], param_modes[2]),
                Parameter::new(self.mem[pc + 2], param_modes[1]),
                Parameter::new(self.mem[pc + 3], param_modes[0]),
            ),
            "02" => Instruction::MUL(
                Parameter::new(self.mem[pc + 1], param_modes[2]),
                Parameter::new(self.mem[pc + 2], param_modes[1]),
                Parameter::new(self.mem[pc + 3], param_modes[0]),
            ),
            "03" => Instruction::ST(Parameter::new(self.mem[pc + 1], param_modes[2])),
            "04" => Instruction::LD(Parameter::new(self.mem[pc + 1], param_modes[2])),
            "05" => Instruction::JNZ(
                Parameter::new(self.mem[pc + 1], param_modes[2]),
                Parameter::new(self.mem[pc + 2], param_modes[1]),
            ),
            "06" => Instruction::JZ(
                Parameter::new(self.mem[pc + 1], param_modes[2]),
                Parameter::new(self.mem[pc + 2], param_modes[1]),
            ),
            "07" => Instruction::STL(
                Parameter::new(self.mem[pc + 1], param_modes[2]),
                Parameter::new(self.mem[pc + 2], param_modes[1]),
                Parameter::new(self.mem[pc + 3], param_modes[0]),
            ),
            "08" => Instruction::STE(
                Parameter::new(self.mem[pc + 1], param_modes[2]),
                Parameter::new(self.mem[pc + 2], param_modes[1]),
                Parameter::new(self.mem[pc + 3], param_modes[0]),
            ),
            "99" => Instruction::HALT,
            _ => unreachable!(),
        }
    }

    fn run(&mut self) {
        let mut instruction = self.fetch();
        while instruction != Instruction::HALT {
            instruction.exec(self);
            instruction = self.fetch();
        }
        println!("Program completed!");
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
enum ParameterMode {
    Position,
    Immediate,
}

#[derive(Debug, PartialEq)]
struct Parameter {
    val: i32,
    mode: ParameterMode,
}

impl Parameter {
    fn new(val: i32, mode: ParameterMode) -> Self {
        Self { val, mode }
    }

    fn load(&self, mem: &[i32]) -> i32 {
        match self.mode {
            ParameterMode::Position => mem[self.val as usize],
            ParameterMode::Immediate => self.val,
        }
    }
}

#[derive(Debug, PartialEq)]
enum Instruction {
    // Part 1
    ADD(Parameter, Parameter, Parameter),
    MUL(Parameter, Parameter, Parameter),
    ST(Parameter),
    LD(Parameter),
    HALT,
    // Part 2
    JNZ(Parameter, Parameter),
    JZ(Parameter, Parameter),
    STL(Parameter, Parameter, Parameter),
    STE(Parameter, Parameter, Parameter),
}

impl Instruction {
    fn exec(&self, program: &mut Program) {
        use Instruction::*;

        match self {
            ADD(p1, p2, p3) => {
                program.mem[p3.val as usize] = p1.load(&program.mem) + p2.load(&program.mem);
                program.pc += 4;
            }
            MUL(p1, p2, p3) => {
                program.mem[p3.val as usize] = p1.load(&program.mem) * p2.load(&program.mem);
                program.pc += 4;
            }
            ST(p1) => {
                program.mem[p1.val as usize] = program.input;
                program.pc += 2;
            }
            LD(p1) => {
                println!(
                    "LD: {}",
                    match p1.mode {
                        ParameterMode::Position => program.mem[p1.val as usize],
                        ParameterMode::Immediate => p1.val,
                    }
                );
                program.pc += 2;
            }
            HALT => {}
            JNZ(p1, p2) => {
                if p1.load(&program.mem) != 0 {
                    program.pc = p2.load(&program.mem) as usize;
                } else {
                    program.pc += 3;
                }
            }
            JZ(p1, p2) => {
                if p1.load(&program.mem) == 0 {
                    program.pc = p2.load(&program.mem) as usize;
                } else {
                    program.pc += 3;
                }
            }
            STL(p1, p2, p3) => {
                let store_addr = p3.val as usize;
                if p1.load(&program.mem) < p2.load(&program.mem) {
                    program.mem[store_addr] = 1;
                } else {
                    program.mem[store_addr] = 0;
                }
                program.pc += 4;
            }
            STE(p1, p2, p3) => {
                let store_addr = p3.val as usize;
                if p1.load(&program.mem) == p2.load(&program.mem) {
                    program.mem[store_addr] = 1;
                } else {
                    program.mem[store_addr] = 0;
                }
                program.pc += 4;
            }
        }
    }
}
