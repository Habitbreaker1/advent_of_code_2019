extern crate num;

use num::Integer;

use std::collections::HashSet;
use std::fs::File;
use std::io::prelude::*;
use std::io::Result;

fn main() -> Result<()> {
    let mut file = File::open("input.txt")?;
    let mut buffer = String::new();
    file.read_to_string(&mut buffer)?;

    let asteroids: Vec<Point<i32>> = buffer
        .trim()
        .lines()
        .enumerate()
        .map(|(y, line)| {
            line.chars()
                .enumerate()
                .filter_map(|(x, c)| {
                    if c == '#' {
                        Some(Point::new(x as i32, y as i32))
                    } else {
                        None
                    }
                })
                .collect::<Vec<Point<i32>>>()
        })
        .flatten()
        .collect();

    let stations: Vec<(&Point<i32>, HashSet<Point<i32>>)> = asteroids
        .iter()
        .map(|station| (station, detect_asteroids(&station, &asteroids)))
        .collect();

    let best_station = stations
        .iter()
        .max_by(|a, b| a.1.len().cmp(&b.1.len()))
        .unwrap();

    println!(
        "Best station: {:?} with detection of {} asteroids",
        best_station.0,
        best_station.1.len()
    );

    // Part 2
    let mut destroyed_ordered: Vec<Point<i32>> = Vec::new();
    let station = best_station.0;
    let mut asteroids_remaining: Vec<Point<i32>> = asteroids
        .clone()
        .into_iter()
        .filter(|a| a != station)
        .collect();

    while asteroids_remaining.len() > 0 {
        let to_destroy = detect_asteroids(&station, &asteroids_remaining);

        let mut destroyed = to_destroy
            .iter()
            .cloned()
            .map(|p| {
                let dx = (p.x - station.x) as f32;
                let dy = (p.y - station.y) as f32;
                // Laser starts at PI/2
                let mut tan_alpha = dy.atan2(dx) + std::f32::consts::PI / 2.0;
                if tan_alpha < 0.0 {
                    tan_alpha += 2.0 * std::f32::consts::PI;
                }
                (p, tan_alpha)
            })
            .collect::<Vec<_>>();

        destroyed.sort_by(|a, b| a.1.partial_cmp(&b.1).unwrap());
        destroyed.iter().for_each(|p| destroyed_ordered.push(p.0));

        asteroids_remaining = asteroids_remaining
            .into_iter()
            .filter(|a| destroyed.iter().find(|d| d.0 == *a).is_none())
            .collect();
    }
    println!(
        "200th destroyed Asteroid: {:?} - 100 * x + y = {:?}",
        destroyed_ordered.get(199),
        {
            if let Some(asteroid) = destroyed_ordered.get(199) {
                Some(asteroid.x * 100 + asteroid.y)
            } else {
                None
            }
        }
    );
    Ok(())
}

fn detect_asteroids(station: &Point<i32>, asteroids: &Vec<Point<i32>>) -> HashSet<Point<i32>> {
    let mut seen_points = HashSet::new();

    for curr_asteroid in asteroids {
        // Don't count station as asteroid
        if *station == *curr_asteroid {
            continue;
        }

        let dx = curr_asteroid.x - station.x;
        let dy = curr_asteroid.y - station.y;

        let gcd = dx.gcd(&dy);

        let mv_x = dx / gcd;
        let mv_y = dy / gcd;

        for step in 1..=gcd {
            let curr_point = Point::new(station.x + mv_x * step, station.y + mv_y * step);
            if let Some(point) = asteroids.iter().find(|ast| **ast == curr_point) {
                seen_points.insert(*point);
                break;
            }
        }
    }
    seen_points
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
struct Point<T: Integer + Clone + Copy> {
    x: T,
    y: T,
}

impl<T: Integer + Clone + Copy> Point<T> {
    fn new(x: T, y: T) -> Self {
        Self { x, y }
    }
}
