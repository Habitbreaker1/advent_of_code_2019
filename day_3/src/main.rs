use rayon::prelude::*;
use std::fs::File;
use std::io::prelude::*;
use std::io::Result as IOResult;

fn main() -> IOResult<()> {
    let mut file = File::open("input.txt")?;

    let mut buffer = String::new();
    file.read_to_string(&mut buffer)?;

    let wires: Vec<_> = buffer.split("\n").collect();

    // Transform Strings into Vec<Move>
    let movement_1 = into_move(&wires[0]);
    let movement_2 = into_move(&wires[1]);

    // Get borders of both wires
    let borders_1 = calc_borders(&movement_1);
    let borders_2 = calc_borders(&movement_2);

    println!("Borders: {:#?}", borders_1);
    println!("Borders: {:#?}", borders_2);

    // Combine both borders to eliminate all points outside range (eliminates ~134000 points)
    let borders_combined = Borders {
        x_min: *[borders_1.x_min, borders_2.x_min].iter().max().unwrap(),
        x_max: *[borders_1.x_max, borders_2.x_max].iter().min().unwrap(),
        y_min: *[borders_1.y_min, borders_2.y_min].iter().max().unwrap(),
        y_max: *[borders_1.y_max, borders_2.y_max].iter().min().unwrap(),
    };
    println!("Combined borders: {:#?}", borders_combined);

    // Save all coords (which are in possible intersection area) in array
    let points_1 = get_wire_coords_optimized(&movement_1, &borders_combined);
    let points_2 = get_wire_coords_optimized(&movement_2, &borders_combined);
    println!(
        "Number of points with elimination (wire 1): {}",
        points_1.len()
    );
    println!(
        "Number of points with elimination (wire 2): {}",
        points_2.len()
    );

    // Get intersection points
    // O(n²) runtime but it gets the job done (hopefully)
    let intersections: Vec<(i32, i32)> = points_1
        .into_par_iter()
        .filter(|p| points_2.contains(&p))
        .collect();
    println!("Number of intersections: {}", intersections.len());

    // Calculate closest intersection point(s)
    let closest_distance = intersections
        .iter()
        .map(|p| (0 - p.0).abs() + (0 - p.1).abs())
        .min();
    println!("Closest distance {:?}", closest_distance);

    // Part 2

    // Get all points
    let points_1 = get_wire_coords(&movement_1);
    let points_2 = get_wire_coords(&movement_2);

    // Find all steps for every intersection
    let steps: Vec<((i32, i32), usize)> = intersections
        .iter()
        .map(|&intersection| {
            (
                intersection,
                count_steps_to_point(&points_1, intersection)
                    + count_steps_to_point(&points_2, intersection),
            )
        })
        .collect();

    // Get point with lowest number of steps
    println!(
        "Best intersection point: {:?}",
        steps.iter().min_by(|x, y| x.1.cmp(&y.1)).unwrap()
    );

    Ok(())
}

fn count_steps_to_point(wire: &[(i32, i32)], dest: (i32, i32)) -> usize {
    wire.iter()
        .position(|&p| p.0 == dest.0 && p.1 == dest.1)
        .unwrap()
        + 1 // Step from (0,0) to first point
}

fn into_move(wire: &str) -> Vec<Move> {
    wire.split(",")
        .map(|mv| Move::from_str(&mv).unwrap())
        .collect()
}

fn get_wire_coords_optimized(instructions: &[Move], borders: &Borders) -> Vec<(i32, i32)> {
    let mut point = (0, 0);
    let coords = instructions
        .iter()
        .map(|instr| {
            instr.mv(&mut point);
            instr.get_points_between_optimized(point, borders)
        })
        .flatten()
        .collect();

    coords
}
fn get_wire_coords(instructions: &[Move]) -> Vec<(i32, i32)> {
    let mut point = (0, 0);
    let coords = instructions
        .iter()
        .map(|instr| {
            instr.mv(&mut point);
            instr.get_points_between(point)
        })
        .flatten()
        .collect();

    coords
}

#[derive(Debug)]
struct Borders {
    x_min: i32,
    x_max: i32,
    y_min: i32,
    y_max: i32,
}

fn calc_borders(points: &[Move]) -> Borders {
    let mut x_min = 0;
    let mut x_max = 0;
    let mut y_min = 0;
    let mut y_max = 0;

    let mut x_curr = 0;
    let mut y_curr = 0;

    for point in points {
        match point {
            Move::R(amount) => {
                x_curr += amount;
            }
            Move::L(amount) => {
                x_curr -= amount;
            }
            Move::U(amount) => {
                y_curr += amount;
            }
            Move::D(amount) => {
                y_curr -= amount;
            }
        };
        if x_curr < x_min {
            x_min = x_curr;
        }
        if y_curr < y_min {
            y_min = y_curr
        }
        if x_curr > x_max {
            x_max = x_curr;
        }
        if y_curr > y_max {
            y_max = y_curr
        }
    }

    Borders {
        x_min,
        x_max,
        y_min,
        y_max,
    }
}

#[derive(Debug)]
enum Move {
    R(i32),
    L(i32),
    U(i32),
    D(i32),
}

use std::num::ParseIntError;
use std::str::FromStr;
impl FromStr for Move {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let instruction = &s[..1];
        let amount = s[1..].parse()?;

        Ok(match instruction {
            "R" => Move::R(amount),
            "L" => Move::L(amount),
            "U" => Move::U(amount),
            "D" => Move::D(amount),
            _ => unreachable!(),
        })
    }
}

impl Move {
    fn mv(&self, point: &mut (i32, i32)) {
        match *self {
            Move::R(amount) => {
                point.0 += amount;
            }
            Move::L(amount) => {
                point.0 -= amount;
            }
            Move::U(amount) => {
                point.1 += amount;
            }
            Move::D(amount) => {
                point.1 -= amount;
            }
        };
    }
    fn get_points_between(&self, point: (i32, i32)) -> Vec<(i32, i32)> {
        match *self {
            Move::R(amount) => (1..=amount as usize)
                .map(|x| (x as i32 + point.0 - amount, point.1))
                .collect(),
            Move::L(amount) => (1..=amount as usize)
                .map(|x| (point.0 + amount - x as i32, point.1))
                .collect(),
            Move::U(amount) => (1..=amount as usize)
                .map(|y| (point.0, y as i32 + point.1 - amount))
                .collect(),
            Move::D(amount) => (1..=amount as usize)
                .map(|y| (point.0, point.1 + amount - y as i32))
                .collect(),
        }
    }

    fn get_points_between_optimized(
        &self,
        point: (i32, i32),
        borders: &Borders,
    ) -> Vec<(i32, i32)> {
        let points: Vec<(i32, i32)> = match *self {
            Move::R(amount) => (1..=amount as usize)
                .map(|x| (x as i32 + point.0 - amount, point.1))
                .collect(),
            Move::L(amount) => (1..=amount as usize)
                .map(|x| (point.0 + amount - x as i32, point.1))
                .collect(),
            Move::U(amount) => (1..=amount as usize)
                .map(|y| (point.0, y as i32 + point.1 - amount))
                .collect(),
            Move::D(amount) => (1..=amount as usize)
                .map(|y| (point.0, point.1 + amount - y as i32))
                .collect(),
        };

        points
            .into_iter()
            .filter(|p| {
                p.0 >= borders.x_min
                    && p.0 <= borders.x_max
                    && p.1 >= borders.y_min
                    && p.1 <= borders.y_max
            })
            .collect()
    }
}
