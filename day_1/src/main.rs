use std::fs::File;
use std::io::prelude::*;
use std::io::Result;

fn main() -> Result<()> {
    let mut file = File::open("input.txt")?;

    let mut buffer = String::new();
    file.read_to_string(&mut buffer)?;

    // Part 1
    let sum: f32 = buffer
        .lines()
        .map(|mass| calc_fuel(mass.parse::<f32>().unwrap()))
        .sum();
    println!("Total fuel needed: {}", sum);

    // Part 2
    let sum: f32 = buffer
        .lines()
        .map(|mass| {
            let mut fuel_module = 0f32;
            let mut mass_remaining = mass.parse::<f32>().unwrap();

            loop {
                let fuel = calc_fuel(mass_remaining);
                if fuel <= 0.0 {
                    break;
                }
                fuel_module += fuel;
                mass_remaining = fuel;
            }
            fuel_module
        })
        .sum();

    println!("Fuel with additional fuel needed: {}", sum);
    Ok(())
}

fn calc_fuel(mass: f32) -> f32 {
    (mass / 3.0).trunc() - 2.0
}
