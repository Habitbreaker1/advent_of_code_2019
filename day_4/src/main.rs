#![feature(is_sorted)]
use rayon::prelude::*;

fn main() {
    let start = 153517 as usize;
    let end = 630395 as usize;

    let possible_pwds: usize = (start..=end)
        .into_par_iter()
        .map(|pwd| number_to_vec(pwd))
        //.filter(|pwd| does_meet_rule_3(pwd) && does_meet_rule_2(pwd)) // Part 1
        .filter(|pwd| does_meet_rule_3(pwd) && does_meet_rule_2_extended(pwd)) // Part 2
        .count();

    println!("Possible passwords: {}", possible_pwds);
}

fn number_to_vec(n: usize) -> Vec<u32> {
    n.to_string()
        .chars()
        .map(|c| c.to_digit(10).unwrap())
        .collect()
}

// Rule 2: Two adjacent digits are the same (like 22 in 122345)
#[allow(dead_code)]
fn does_meet_rule_2(arr: &[u32]) -> bool {
    arr.windows(2).any(|c| c[0] == c[1])
}

fn does_meet_rule_2_extended(arr: &[u32]) -> bool {
    use std::collections::HashSet;

    // Get unique indices of multi-appearing numbers
    let appearances: HashSet<Vec<usize>> = arr
        .iter()
        .map(|n| {
            arr.iter()
                .enumerate()
                .filter(|(_, el)| **el == *n)
                .map(|tuple| tuple.0)
                .collect()
        })
        .collect();

    // Get comb of 2-pairs
    let pairs_of_two: Vec<&[usize]> = appearances
        .iter()
        .map(|a| a.windows(2).collect::<Vec<_>>())
        .flatten()
        .collect();

    // Get comb of 3-pairs
    let pairs_of_three: Vec<&[usize]> = appearances
        .iter()
        .map(|a| a.windows(3).collect::<Vec<_>>())
        .flatten()
        .collect();

    let pairs_of_three_connected: Vec<_> = pairs_of_three
        .iter()
        .filter(|c| c[1] == c[0] + 1 && c[2] == c[1] + 1)
        .collect();

    match pairs_of_three_connected.len() {
        0 => pairs_of_two.len() > 0,
        len => pairs_of_two.iter().any(|p| {
            p.iter()
                .all(|n| (0..len).all(|i| !pairs_of_three_connected[i].contains(n)))
        }),
    }
}

// Rule 3: Going from left to right, the digits never decrease; they only ever increase or stay the
// same (like 111123 or 135679)
fn does_meet_rule_3(arr: &[u32]) -> bool {
    arr.is_sorted()
}
