use std::fs::File;
use std::io::prelude::*;
use std::io::Result;

fn main() -> Result<()> {
    let mut file = File::open("test2.txt")?;
    let mut buffer = String::new();
    file.read_to_string(&mut buffer)?;

    let orbits: Vec<(&str, &str)> = buffer
        .lines()
        .map(|l: &str| {
            let split: Vec<&str> = l.split(")").collect();
            (split[0], split[1])
        })
        .collect();

    let mut com = Object::center_of_mass();
    com.append_orbits(&mut orbits.clone());

    let you = Object::new("B".into(), 0);
    let san = Object::new("F".into(), 0);
    println!(
        "Distance between {} and {}: {}",
        you.ident,
        san.ident,
        you.find_distance(&san, &orbits)
    );
    println!("Checksum {}", com.checksum());

    Ok(())
}

#[derive(Debug, PartialEq, Clone)]
struct Object {
    ident: String,
    // Distance from every child to center of mass
    center_dist: usize,
    children: Vec<Object>,
}

impl Object {
    fn new(ident: String, center_dist: usize) -> Self {
        Self {
            ident,
            center_dist,
            children: Vec::new(),
        }
    }

    fn find_distance(&self, other: &Self, orbits: &[(&str, &str)]) -> usize {
        use std::collections::HashMap;
        // find parent of orbiting object (note: an objet can only orbit 1 other object so there can be only
        // one parent
        let get_parent = |ident_orbiting: &str| {
            orbits
                .iter()
                .find(|o| o.1 == ident_orbiting)
                .map(|parent| parent.0)
        };

        let mut curr_self = self.ident.clone();
        let mut curr_other = other.ident.clone();

        let mut self_passed_orbits = HashMap::new();
        let mut other_passed_orbits = HashMap::new();

        loop {
            if let Some(parent) = get_parent(&curr_self) {
                let distance = self_passed_orbits.len();
                self_passed_orbits.insert(parent, distance);

                if let Some(other_dist) = other_passed_orbits.get(&parent) {
                    break distance + other_dist;
                }
                curr_self = parent.to_string();
            }
            if let Some(parent) = get_parent(&curr_other) {
                let distance = other_passed_orbits.len();
                other_passed_orbits.insert(parent, distance);
                if let Some(self_dist) = self_passed_orbits.get(&parent) {
                    break distance + self_dist;
                }
                curr_other = parent.to_string();
            }
        }
    }

    fn center_of_mass() -> Self {
        Self {
            ident: "COM".to_string(),
            center_dist: 1,
            children: Vec::new(),
        }
    }

    fn append_orbits(&mut self, orbits: &mut Vec<(&str, &str)>) {
        let children: Vec<_> = orbits
            .iter()
            .enumerate()
            .filter(|(_, o)| o.0 == self.ident)
            .map(|child| {
                let object = child.1;
                (child.0, object.1)
            })
            .collect();

        // Remove elements which are already in tree so that the remaining orbit size
        // shrinks on every call
        for (idx, child) in children.iter().enumerate() {
            orbits.remove(child.0 - idx);
        }

        for child in children {
            let mut new = Object::new(child.1.to_string(), self.center_dist + 1);
            new.append_orbits(orbits);
            self.children.push(new);
        }
    }

    fn checksum(&self /*,offset: usize*/) -> usize {
        let mut sum = self.children.len() * self.center_dist;

        sum += self
            .children
            .iter()
            .map(|child| child.checksum(/*offset + 1*/))
            .sum::<usize>();

        sum
    }
}
