mod instruction_set;
mod program;

use std::fs::File;
use std::io::prelude::*;
use std::io::Result;

use program::{Int, Program};

fn main() -> Result<()> {
    let mut file = File::open("input.txt")?;
    let mut buffer = String::new();
    file.read_to_string(&mut buffer)?;

    let mem: Vec<Int> = buffer
        .trim()
        .split(",")
        .map(|opcode| opcode.parse().unwrap())
        .collect();

    let mut program = Program::new(mem.clone(), vec![1]);
    println!("Output (part 1): {:?}", program.run());

    program = Program::new(mem.clone(), vec![2]);
    println!("Output (part 2): {:?}", program.run());

    Ok(())
}
