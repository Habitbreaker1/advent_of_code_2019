use image;
use std::fs::File;
use std::io::prelude::*;
use std::io::Result;

fn main() -> Result<()> {
    let mut file = File::open("input.txt")?;
    let mut buffer = String::new();
    file.read_to_string(&mut buffer)?;

    let dimensions = (25, 6);

    let pixels: Vec<u32> = buffer
        .chars()
        .map(|c| c.to_digit(10).unwrap())
        .collect::<Vec<u32>>();

    let layers: Vec<_> = pixels
        .chunks(dimensions.0 * dimensions.1)
        .collect::<Vec<_>>()
        .to_vec()
        .into_iter()
        .enumerate()
        .map(|(idx, chunk)| {
            (
                idx,
                chunk,
                chunk.iter().filter(|digit| **digit == 0).count(),
            )
        })
        .collect();
    let layer_fewest_zeros = layers.iter().min_by(|x, y| x.2.cmp(&y.2));

    if let Some(layer) = layer_fewest_zeros {
        println!("Layer with fewest zeros: {:?}", layer.0 + 1);
        println!("n(1s) * n(2s) = {}", {
            let tuple = layer.1.iter().fold((0, 0), |mut acc, pixel| {
                match pixel {
                    1 => acc.0 += 1,
                    2 => acc.1 += 1,
                    _ => {}
                };
                acc
            });

            tuple.0 * tuple.1
        });
    }

    let final_layer: Vec<u32> = (0..(dimensions.0 * dimensions.1))
        .map(|pixel| {
            layers
                .iter()
                .find(|layer| layer.1[pixel] != 2)
                .map(|visible_layer| visible_layer.1[pixel])
                .unwrap()
        })
        .collect();

    let mut imgbuf = image::ImageBuffer::new(dimensions.0 as u32, dimensions.1 as u32);

    for (x, y, pixel) in imgbuf.enumerate_pixels_mut() {
        let white_or_black = (final_layer[(y * dimensions.0 as u32 + x) as usize] * 255) as u8;
        *pixel = image::Rgb([white_or_black, white_or_black, white_or_black]);
    }

    imgbuf.save("out.png").unwrap();

    Ok(())
}
