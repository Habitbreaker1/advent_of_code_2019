mod program;

use program::Program;
use std::fs::File;
use std::io::prelude::*;
use std::io::Result;

//     O-------O  O-------O  O-------O  O-------O  O-------O
// 0 ->| Amp A |->| Amp B |->| Amp C |->| Amp D |->| Amp E |-> (to thrusters)
//     O-------O  O-------O  O-------O  O-------O  O-------O
fn main() -> Result<()> {
    let mut file = File::open("input.txt")?;
    let mut buffer = String::new();
    file.read_to_string(&mut buffer)?;

    let mem: Vec<i32> = buffer
        .split(",")
        .map(|opcode| opcode.parse().unwrap())
        .collect();

    let mut max_out: (Option<i32>, Vec<usize>) = (None, Vec::with_capacity(5));

    // --------------------------- Part 1 -----------------------
    for seq_0 in 0..5 {
        for seq_1 in 0..5 {
            for seq_2 in 0..5 {
                for seq_3 in 0..5 {
                    for seq_4 in 0..5 {
                        let mut hs = std::collections::HashSet::new();
                        hs.insert(seq_0);
                        hs.insert(seq_1);
                        hs.insert(seq_2);
                        hs.insert(seq_3);
                        hs.insert(seq_4);
                        if hs.len() != 5 {
                            continue;
                        }
                        let out_a = Program::new(mem.clone(), vec![0, seq_0 as i32]).run();
                        let out_b = Program::new(mem.clone(), vec![out_a, seq_1 as i32]).run();
                        let out_c = Program::new(mem.clone(), vec![out_b, seq_2 as i32]).run();
                        let out_d = Program::new(mem.clone(), vec![out_c, seq_3 as i32]).run();
                        let out_e = Program::new(mem.clone(), vec![out_d, seq_4 as i32]).run();

                        if let Some(max) = max_out.0 {
                            if out_e > max {
                                max_out.0 = Some(out_e);
                                max_out.1[0] = seq_0;
                                max_out.1[1] = seq_1;
                                max_out.1[2] = seq_2;
                                max_out.1[3] = seq_3;
                                max_out.1[4] = seq_4;
                            }
                        } else {
                            max_out.0 = Some(out_e);
                            max_out.1.push(seq_0);
                            max_out.1.push(seq_1);
                            max_out.1.push(seq_2);
                            max_out.1.push(seq_3);
                            max_out.1.push(seq_4);
                        }
                    }
                }
            }
        }
    }

    println!(
        "Max signal (part 1): {}, Seq: {:?}",
        max_out.0.unwrap(),
        max_out.1
    );

    // --------------------------- Part 2 -----------------------
    for seq_0 in 5..10 {
        for seq_1 in 5..10 {
            for seq_2 in 5..10 {
                for seq_3 in 5..10 {
                    for seq_4 in 5..10 {
                        let mut hs = std::collections::HashSet::new();
                        hs.insert(seq_0);
                        hs.insert(seq_1);
                        hs.insert(seq_2);
                        hs.insert(seq_3);
                        hs.insert(seq_4);
                        if hs.len() != 5 {
                            continue;
                        }

                        // Initialize with 0
                        let mut out_e = (false, 0);

                        // Create initial states of amplifiers with start sequence.
                        // States of amplifiers (program counter, memory) don't get
                        // reinitialized during loop progression.
                        let mut amp_a = Program::new(mem.clone(), vec![out_e.1, seq_0 as i32]);
                        let out_a = amp_a.run_loop();

                        let mut amp_b = Program::new(mem.clone(), vec![out_a.1, seq_1 as i32]);
                        let out_b = amp_b.run_loop();

                        let mut amp_c = Program::new(mem.clone(), vec![out_b.1, seq_2 as i32]);
                        let out_c = amp_c.run_loop();

                        let mut amp_d = Program::new(mem.clone(), vec![out_c.1, seq_3 as i32]);
                        let out_d = amp_d.run_loop();

                        let mut amp_e = Program::new(mem.clone(), vec![out_d.1, seq_4 as i32]);
                        out_e = amp_e.run_loop();

                        if let Some(max) = max_out.0 {
                            if out_e.1 > max {
                                max_out.0 = Some(out_e.1);
                                max_out.1[0] = seq_0;
                                max_out.1[1] = seq_1;
                                max_out.1[2] = seq_2;
                                max_out.1[3] = seq_3;
                                max_out.1[4] = seq_4;
                            }
                        } else {
                            max_out.0 = Some(out_e.1);
                            max_out.1.push(seq_0);
                            max_out.1.push(seq_1);
                            max_out.1.push(seq_2);
                            max_out.1.push(seq_3);
                            max_out.1.push(seq_4);
                        }

                        let mut not_halted = !out_e.0;

                        while not_halted {
                            amp_a.add_param(out_e.1);
                            let out_a = amp_a.run_loop();

                            amp_b.add_param(out_a.1);
                            let out_b = amp_b.run_loop();

                            amp_c.add_param(out_b.1);
                            let out_c = amp_c.run_loop();

                            amp_d.add_param(out_c.1);
                            let out_d = amp_d.run_loop();

                            amp_e.add_param(out_d.1);
                            out_e = amp_e.run_loop();

                            if let Some(max) = max_out.0 {
                                if out_e.1 > max {
                                    max_out.0 = Some(out_e.1);
                                    max_out.1[0] = seq_0;
                                    max_out.1[1] = seq_1;
                                    max_out.1[2] = seq_2;
                                    max_out.1[3] = seq_3;
                                    max_out.1[4] = seq_4;
                                }
                            } else {
                                max_out.0 = Some(out_e.1);
                                max_out.1.push(seq_0);
                                max_out.1.push(seq_1);
                                max_out.1.push(seq_2);
                                max_out.1.push(seq_3);
                                max_out.1.push(seq_4);
                            }

                            not_halted = !out_e.0;
                        }
                    }
                }
            }
        }
    }
    println!(
        "Max signal (part 2): {}, Seq: {:?}",
        max_out.0.unwrap(),
        max_out.1
    );

    Ok(())
}
